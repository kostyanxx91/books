import {observer} from "mobx-react";
import React from "react";
import {AppContext} from "../stores/Stores";
import Add from '@material-ui/icons/Add';
import Delete from '@material-ui/icons/Clear';
import Edit from '@material-ui/icons/Edit';
import Loader from './ui/loader';
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import TextField from '@material-ui/core/TextField';
import {Field, Form, Formik} from "formik";

const Authors = observer(() => {
    const {pageStore, authorStore} = React.useContext(AppContext);

    if (pageStore.loading) return <Loader/>;

    return (
        <div>
            <div className="container">
                <div className="row">
                    <div className="col p-3">
                        <h1>Авторы</h1>
                    </div>
                </div>
                <div className="row">
                    <div className="col p-3">
                        <Add className="cursor-pointer"
                             onClick={() => authorStore.openModal()}/>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <table className="table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>ФАМИЛИЯ</th>
                                <th>ИМЯ</th>
                                <th>КОЛЛИЧЕСТВО КНИГ</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            {authorStore.authors.map((row) => (
                                <tr key={row.id}>
                                    <td>{row.id}</td>
                                    <td>{row.first_name}</td>
                                    <td>{row.last_name}</td>
                                    <td>{row.books_count}</td>
                                    <td className="text-right">
                                        <Edit className="cursor-pointer ml-2" onClick={() => {
                                            const author = authorStore.authors.find((item) => item.id === row.id);
                                            authorStore.openModal();
                                            authorStore.setFormData(author);
                                            authorStore.setUpdatedId(row.id);
                                        }}/>
                                        <Delete
                                            className="cursor-pointer ml-2"
                                            onClick={() => authorStore.delete(row.id)}/>
                                    </td>
                                </tr>
                            ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            {authorStore.modal && (
                <Dialog
                    open={true}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description">
                    <Formik
                        initialValues={authorStore.formData}
                        onSubmit={(values) => {
                            authorStore.save({
                                first_name: values.first_name,
                                last_name: values.last_name,
                            });
                        }}
                        render={() => (
                            <Form translate="yes">
                                <div style={{width: 450}}>
                                    <DialogContent>
                                        <div className="container">
                                            <div className="row mt-3">
                                                <div className="col">
                                                    <Field
                                                        name="first_name"
                                                        render={(rf) => (
                                                            <div>
                                                                <label style={{color: 'red'}}>
                                                                    {authorStore.formErrors['first_name']}
                                                                </label>
                                                                <TextField
                                                                    className="w-100"
                                                                    {...rf.field} label="Фамилия"/>
                                                            </div>
                                                        )}
                                                    />
                                                </div>
                                            </div>
                                            <div className="row mt-3">
                                                <div className="col">
                                                    <Field
                                                        name="last_name"
                                                        render={(rf) => (
                                                            <div>
                                                                <label style={{color: 'red'}}>
                                                                    {authorStore.formErrors['last_name']}
                                                                </label>
                                                                <TextField
                                                                    className="w-100"
                                                                    {...rf.field} label="Имя"/>
                                                            </div>
                                                        )}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </DialogContent>
                                    <DialogActions className="p-3 mt-2">
                                        <Button
                                            color="primary"
                                            onClick={() => authorStore.closeModal()}
                                            className="modal-button-cancel">
                                            ОТМЕНА
                                        </Button>
                                        <Button
                                            color="primary"
                                            type="submit"
                                            className="modal-button-cancel">
                                            СОХРАНИТЬ
                                        </Button>
                                    </DialogActions>
                                </div>
                            </Form>
                        )}
                    />
                </Dialog>)}
        </div>
    );
});

export default Authors;
