import React from "react";
import CircularProgress from '@material-ui/core/CircularProgress';

const Loader = () => (
    <div>
        <div className="loader">
            <div className="loader-box">
                <CircularProgress size={70}/>
            </div>
        </div>
    </div>
);

export default Loader;