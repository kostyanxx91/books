import {observer} from "mobx-react";
import React from "react";
import {AppContext} from "../stores/Stores";
import Add from '@material-ui/icons/Add';
import Delete from '@material-ui/icons/Clear';
import Edit from '@material-ui/icons/Edit';
import Loader from './ui/loader';
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import {Field, Form, Formik} from "formik";

const Books = observer(() => {
    const {pageStore, bookStore} = React.useContext(AppContext);

    if (pageStore.loading) return <Loader/>;

    return (
        <div>
            <div className="container">
                <div className="row">
                    <div className="col p-3">
                        <h1>Книги</h1>
                    </div>
                </div>
                <div className="row">
                    <div className="col p-3">
                        <Add className="cursor-pointer"
                             onClick={() => bookStore.openModal()}/>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <table className="table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>НАЗВАНИЕ</th>
                                <th>ОПИСАНИЕ</th>
                                <th>АВТОР</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            {bookStore.books.map((row) => (
                                <tr key={row.id}>
                                    <td>{row.id}</td>
                                    <td>{row.title}</td>
                                    <td>{row.description}</td>
                                    <td>
                                        {row.author.first_name} {row.author.last_name}
                                    </td>
                                    <td className="text-right">
                                        <Edit className="cursor-pointer ml-2" onClick={() => {
                                            const book = bookStore.books.find((item) => item.id === row.id);
                                            bookStore.openModal();
                                            bookStore.setFormData(book);
                                            bookStore.setUpdatedId(row.id);
                                        }}/>
                                        <Delete
                                            className="cursor-pointer ml-2"
                                            onClick={() => bookStore.delete(row.id)}/>
                                    </td>
                                </tr>
                            ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            {bookStore.modal && (
                <Dialog
                    open={true}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description">
                    <Formik
                        initialValues={bookStore.formData}
                        onSubmit={(values) => {
                            bookStore.save({
                                title: values.title,
                                description: values.description,
                                author_id: values.author_id
                            });
                        }}
                        render={() => (
                            <Form translate="yes">
                                <div style={{width: 450}}>
                                    <DialogContent>
                                        <div className="container">
                                            <div className="row mt-3">
                                                <div className="col">
                                                    <Field
                                                        name="title"
                                                        render={(rf) => (
                                                            <div>
                                                                <label style={{color: 'red'}}>
                                                                    {bookStore.formErrors['title']}
                                                                </label>
                                                                <TextField
                                                                    className="w-100"
                                                                    {...rf.field} label="Название"/>
                                                            </div>
                                                        )}
                                                    />
                                                </div>
                                            </div>
                                            <div className="row mt-3">
                                                <div className="col">
                                                    <Field
                                                        name="description"
                                                        render={(rf) => (
                                                            <div>
                                                                <label style={{color: 'red'}}>
                                                                    {bookStore.formErrors['description']}
                                                                </label>
                                                                <TextField
                                                                    className="w-100"
                                                                    {...rf.field} label="Описание"/>
                                                            </div>
                                                        )}
                                                    />
                                                </div>
                                            </div>
                                            <div className="row mt-3">
                                                <div className="col">
                                                    <Field
                                                        name="author_id"
                                                        render={(rf) => (
                                                            <div>
                                                                <label style={{color: 'red'}}>
                                                                    {bookStore.formErrors['author_id']}
                                                                </label>
                                                                <Select
                                                                    className='w-100 mt-3'
                                                                    {...rf.field}>
                                                                    <MenuItem value={0}>Выберите автора</MenuItem>
                                                                    {bookStore.authors.map((row) => (
                                                                        <MenuItem key={row.id} value={row.id}>
                                                                            {row.first_name} {row.last_name}
                                                                        </MenuItem>
                                                                    ))}
                                                                </Select>
                                                            </div>
                                                        )}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </DialogContent>
                                    <DialogActions className="p-3 mt-2">
                                        <Button
                                            color="primary"
                                            onClick={() => bookStore.closeModal()}
                                            className="modal-button-cancel">
                                            ОТМЕНА
                                        </Button>
                                        <Button
                                            color="primary"
                                            type="submit"
                                            className="modal-button-cancel">
                                            СОХРАНИТЬ
                                        </Button>
                                    </DialogActions>
                                </div>
                            </Form>
                        )}
                    />
                </Dialog>)}
        </div>
    );
});

export default Books;
