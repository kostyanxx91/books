const httpRequest = (url, method, params) => {
    return fetch(`http://localhost:8000/api/v1/${url}`, {
        method: method,
        headers: {
            'Content-Type': 'application/json',
            'Accept': "application/json"
        },
        body: method !== "GET" ? JSON.stringify(params) : null,
    });
};

export default httpRequest;
