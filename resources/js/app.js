import React from 'react';
import ReactDOM from 'react-dom';
import Authors from './components/authors.js';
import Books from './components/books.js';
import {
    BrowserRouter,
    Route,
    Switch
} from 'react-router-dom';

ReactDOM.render(
    <BrowserRouter>
        <div>
            <Switch>
                <Route exact path="/admin/books" component={Books} />
                <Route exact path="/admin/authors" component={Authors} />
            </Switch>
        </div>
</BrowserRouter>, document.getElementById('root'));