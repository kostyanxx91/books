import {observable, action, toJS} from 'mobx';

class PageStore {
    @observable loading = false;

    constructor() {}

    @action startLoading = () => {
        this.loading = true;
    };

    @action endLoading = () => {
        this.loading = false;
    }
}

export default new PageStore();