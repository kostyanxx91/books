import {observable, action, toJS} from 'mobx';
import pageStore from './PageStore';
import httpRequest from '../utils/request';

class AuthorStore {
    @observable loading = false;
    @observable modal = false;
    @observable authors = [];
    @observable formData = {
        first_name: '',
        last_name: ''
    };
    @observable updatedId = 0;
    @observable formErrors = {};

    constructor() {
        this.fetchAll();
    }

    fetchAll = async () => {
        pageStore.startLoading();

        const response = await httpRequest('authors', 'GET', {});
        const responseData = await response.json();

        if (responseData) {
            this.authors = responseData;
            pageStore.endLoading();
        }
    };

    @action delete = async (id) => {
        pageStore.startLoading();

        const response = await httpRequest('authors/' + id, 'DELETE', {});
        const responseData = await response.json();

        if (responseData && responseData['id']) {
            this.fetchAll();
        }
    };

    @action save = async (author) => {
        let method = 'POST';
        let url = 'authors';

        pageStore.startLoading();

        if (this.updatedId > 0) {
            method = 'PUT';
            url = 'authors/' + this.updatedId;
        }

        const response = await httpRequest(url, method, author);
        const responseData = await response.json();
        this.formData = author;

        if (response.status === 200) {
            if (responseData && responseData['id']) {
                this.modal = false;
                this.fetchAll();
            }
        } else {
            this.formErrors = responseData['errors'];
            pageStore.endLoading();
        }
    };

    @action closeModal = () => {
        this.modal = false;
    };

    @action openModal = () => {
        this.modal = true;
        this.updatedId = 0;
        this.formData = {
            first_name: '',
            last_name: ''
        };
        this.formErrors = {};
    };

    @action setFormData = (data) => {
        this.formData = data;
    };

    @action setUpdatedId = (id) => {
        this.updatedId = id;
    };
}

export default new AuthorStore();