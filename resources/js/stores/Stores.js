import * as React from "react";
import pageStore from './PageStore';
import bookStore from './BookStore';
import authorStore from './AuthorStore';

export function createStores() {
    return {
        pageStore,
        bookStore,
        authorStore
    };
}

export const stores = createStores();

export const AppContext = React.createContext(stores);