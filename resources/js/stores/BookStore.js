import {observable, action, toJS} from 'mobx';
import pageStore from './PageStore';
import httpRequest from '../utils/request';

class BookStore {
    @observable loading = false;
    @observable modal = false;
    @observable books = [];
    @observable authors = [];
    @observable formData = {
        title: '',
        description: '',
        author_id: 0
    };
    @observable updatedId = 0;
    @observable formErrors = {};

    constructor() {
        this.fetchAll();
        this.getAuthors();
    }

    fetchAll = async () => {
        pageStore.startLoading();

        const response = await httpRequest('books', 'GET', {});
        const responseData = await response.json();

        if (responseData) {
            this.books = responseData;
            pageStore.endLoading();
        }
    };

    getAuthors = async () => {
        pageStore.startLoading();

        const response = await httpRequest('authors', 'GET', {});
        const responseData = await response.json();

        if (responseData) {
            this.authors = responseData;
            pageStore.endLoading();
        }
    };

    @action delete = async (id) => {
        pageStore.startLoading();

        const response = await httpRequest('books/' + id, 'DELETE', {});
        const responseData = await response.json();

        if (responseData && responseData['id']) {
            this.fetchAll();
        }
    };

    @action save = async (book) => {
        let method = 'POST';
        let url = 'books';

        pageStore.startLoading();

        if (this.updatedId > 0) {
            method = 'PUT';
            url = 'books/' + this.updatedId;
        }

        const response = await httpRequest(url, method, book);
        const responseData = await response.json();
        this.formData = book;

        if (response.status === 200) {
            if (responseData && responseData['id']) {
                this.modal = false;
                this.fetchAll();
            }
        } else {
            this.formErrors = responseData['errors'];
            pageStore.endLoading();
        }
    };

    @action closeModal = () => {
        this.modal = false;
    };

    @action openModal = () => {
        this.modal = true;
        this.updatedId = 0;
        this.formData = {
            title: '',
            description: '',
            author_id: 0
        };
        this.formErrors = {};
    };

    @action setFormData = (data) => {
        this.formData = data;
    };

    @action setUpdatedId = (id) => {
        this.updatedId = id;
    };
}

export default new BookStore();