<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="{{mix('css/app.css')}}" rel="stylesheet" type="text/css">
        <title>Авторы</title>
    </head>
    <body>
        <div class="container">
            <div class="row mt-4">
                <div class="col">
                    <h1>Авторы</h1>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-2 border-bottom">
                     Имя
                </div>
                <div class="col-2 border-bottom">
                    Фамилия
                </div>
                <div class="col-2 border-bottom">
                    Книги
                </div>
            </div>
            @foreach($authors as $author)
                <div class="row mt-4">
                    <div class="col-2 border-bottom">
                        {{$author->first_name}}
                    </div>
                    <div class="col-2 border-bottom">
                        {{$author->last_name}}
                    </div>
                    <div class="col-2 border-bottom">
                        @foreach($author->books as $book)
                            <label class="ml-2">
                                {{$book->title}}
                            </label>
                        @endforeach
                    </div>
                </div>
            @endforeach
        </div>
    </body>
</html>