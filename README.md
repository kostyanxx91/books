## Настройка .env

- установить настройки с БД
- настроить APP_URL

## Установка

- выполнить composer install
- выполнить php artisan migrate
- выполнить npm install && npm run dev

## Запуск

- выполнить php artisan serve

### Администрирование

- /admin/authors - Администрирование авторов
- /admin/books - Администрирование книг

### Публичная часть

- доступна по роуту /

## API 

### Авторы

- GET /api/v1/authors
- GET /api/v1/authors/{id}
- POST /api/v1/authors
- PUT /api/v1/authors/{id}
- DELETE /api/v1/authors/{id}

### Книги

- GET /api/v1/books
- GET /api/v1/books/{id}
- POST /api/v1/books
- PUT /api/v1/books/{id}
- DELETE /api/v1/books/{id}