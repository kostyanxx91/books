<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\AuthorRequest;
use App\Author;

class AuthorController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $authors = Author::query()
            ->withCount(['books'])
            ->get();

        return response()->json($authors);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $author = Author::query()
            ->whereId($id)
            ->firstOrFail();

        return response()->json($author);
    }

    /**
     * @param AuthorRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function store(AuthorRequest $request)
    {
        $author = new Author();
        $author = $author
            ->fill($request->all());
        $author->saveOrFail();

        return response()->json($author);
    }

    /**
     * @param $id
     * @param AuthorRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, AuthorRequest $request)
    {
        $author = Author::findOrFail($id);
        $author->fill($request->all());
        $author->saveOrFail();

        return response()->json($author);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $author = Author::findOrFail($id);
        $author->delete();

        return response()->json($author);
    }
}