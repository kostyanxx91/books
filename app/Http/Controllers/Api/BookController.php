<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\BookRequest;
use App\Book;

class BookController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $books = Book::query()
            ->with(['author'])
            ->get();

        return response()->json($books);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $book = Book::query()
            ->with(['author'])
            ->whereId($id)
            ->firstOrFail();

        return response()->json($book);
    }

    /**
     * @param BookRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function store(BookRequest $request)
    {
        $book = new Book();
        $book = $book
            ->fill($request->all());
        $book->saveOrFail();

        return response()->json($book);
    }

    /**
     * @param $id
     * @param BookRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, BookRequest $request)
    {
        $book = Book::findOrFail($id);
        $book->fill($request->all());
        $book->saveOrFail();

        return response()->json($book);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $book = Book::findOrFail($id);
        $book->delete();

        return response()->json($book);
    }
}