<?php

namespace App\Http\Controllers;

use App\Author;

class HomeController extends Controller
{
    public function index()
    {
        $authors = Author::query()
            ->with(['books'])
            ->get();

        return view('home')->with('authors', $authors);;
    }
}