<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BookRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'author_id' => ['required', Rule::exists('authors', 'id')],
            'description' => 'required',
            'title' => 'required',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'author_id.exists' => 'Автор не выбран',
            'description.required' => 'Описание - обязательное поле',
            'title.required' => 'Название - обязательное поле',
        ];
    }
}